#include <string.h>
#include <malloc.h>

int to_int(char c) {
  return (int) c - (int) '0';
}

char to_char(int n) {
  return (char) ((int) '0' + n);
}

int add(char a, char b, int carry) {
    return to_int(a) + to_int(b) + carry;
}

int max(int a, int b) {
  return a > b ? a : b;
}

int min(int a, int b) {
  return a < b ? a : b;
}

char *strip_leading_space_and_zeroes(const char *s) {
    char *begin_stripped_str = strpbrk(s, "123456789");
    if (begin_stripped_str == NULL) {
      // all zeroes or blanks!---convert to 0
      begin_stripped_str = "0";
    }

    char *stripped_str = malloc((strlen(begin_stripped_str) + 1) * sizeof (char));
    return strncpy(stripped_str, begin_stripped_str, strlen(begin_stripped_str) + 1);
}

char *init_answer(const char *stripped_a, const char *stripped_b) {

    int len_stripped_a = strlen(stripped_a);
    int len_stripped_b = strlen(stripped_b);

    // allocate memory for the longest possible answer including carry and
    // null terminator
    int max_len_possible = max(len_stripped_a, len_stripped_b);
    char *answer = malloc((max_len_possible + 2) * sizeof (char));

    // initialize answer to the longest input value, adding a 0 to the front
    // to leave room for the carry digit, if necessary
    const char *default_answer = len_stripped_a > len_stripped_b ? stripped_a
                                                                 : stripped_b;
    memcpy(answer + 1, default_answer, max_len_possible + 1);
    memset(answer, '0', 1);

    return answer;
}

char *strsum(const char *a, const char *b)
{
    char *stripped_a = strip_leading_space_and_zeroes(a);
    char *stripped_b = strip_leading_space_and_zeroes(b);
    int len_stripped_a = strlen(stripped_a);
    int len_stripped_b = strlen(stripped_b);


    char *buffer = init_answer(stripped_a, stripped_b);

    // the current digit positions involved in calculation
    // pointers start at the ones' place
    char *answer_digit = buffer + strlen(buffer) - 1;
    const char *stripped_a_digit = stripped_a + len_stripped_a - 1;
    const char *stripped_b_digit = stripped_b + len_stripped_b - 1;

    int digit_pairs_to_add = min(len_stripped_a, len_stripped_b);
    int carry = 0;
    int pair_sum = 0;

    for (int pair = 0; pair < digit_pairs_to_add; pair++) {
        pair_sum = add(*stripped_a_digit, *stripped_b_digit, carry);
        carry = pair_sum / 10;
        *answer_digit = to_char(pair_sum % 10);
        stripped_a_digit--;
        stripped_b_digit--;
        answer_digit--;
    }

    // we've exhausted the digits of the shortest input
    // flush out the carry as far as needed
    while (answer_digit >= buffer) {
        pair_sum = add(*answer_digit, '0', carry);
        carry = pair_sum / 10;
        *answer_digit = to_char(pair_sum % 10);
        answer_digit--;
    }

    char *answer = strip_leading_space_and_zeroes(buffer);
    free(buffer);
    free(stripped_a);
    free(stripped_b);
    return answer;
}
